const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const port = 3000;
const rtAPIv1 = express.Router();
const rpcAPIv2 = express.Router();

let users = [
    {id: 1, name: 'Вася', score: 20},
    {id: 2, name: 'Петя', score: 10},
    {id: 3, name: 'Толя', score: 50}
];

app.use("/api/v1", rtAPIv1);
app.use("/api/v2", rpcAPIv2);
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));
app.use(function (err, req, res, next) {
    // console.error(err.stack);
    res.send('ERROR')
});

// REST
rtAPIv1.get("/users/", function(req, res) {
    let usersLimited = users.slice();
    let limit = req.query.limit;
    let offset = req.query.offset;

    if (limit || offset)
        res.send(usersLimited.splice(offset, limit));
    else
        res.send(users)
});

rtAPIv1.post("/users/", function(req, res) {
    let deleteAll = req.query.deleteAll, id;
    users.length !== 0 ? id = users[users.length - 1].id + 1 : id = 1;

    if (deleteAll === '1')
        users = [];
    else
        users.push({id: id, name: req.query.name, score: req.query.score});
    res.send(users)
});

rtAPIv1.get("/users/:id", function(req, res) {
    let id = req.params.id;
    res.send(users[id - 1])
});

rtAPIv1.put("/users/:id", function(req, res) {
    let id = req.params.id;
    users[id - 1].name = req.query.name;
    users[id - 1].score = req.query.score;
    res.send(users)
});

rtAPIv1.delete("/users/:id", function(req, res) {
    let id = req.params.id;
    delete users[id - 1];
    res.send(users)
});

// RPC
rpcAPIv2.post("/users/*", function(req, res) {
    let method = req.query.method;
    let id = req.query.id;
    let name = req.query.name;
    let score = req.query.score;

    if (method === 'create')
        users.push({id: (users[users.length - 1].id + 1), name: name, score: score});
    if (method === 'read')
        return res.send(users[id - 1]);
    if (method === 'update') {
        users[id - 1].name = name;
        users[id - 1].score = score;
    }
    if (method === 'delete')
        delete users[id - 1];

    res.send(users)
});


app.listen(port, function() {
    console.log(`Listening on port ${port}`);
});
